FROM alpine:3.19.0

# renovate: datasource=repology depName=alpine_3_19/tor versioning=loose
ENV TRANSMISSION_DAEMON_VERSION="4.0.4-r0"

# renovate: datasource=repology depName=alpine_3_19/dumb-init versioning=loose
ENV DUMB_INIT_VERSION="1.2.5-r3"

RUN set -feux \
  && apk --no-cache add transmission-daemon=${TRANSMISSION_DAEMON_VERSION} \
                        dumb-init=${DUMB_INIT_VERSION}

USER transmission

ENTRYPOINT ["/usr/bin/dumb-init", "--"]
CMD ["transmission-daemon", "--foreground", "--auth", "--username=admin", "--password=CHANGEME"]
